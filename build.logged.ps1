# this is the powershell version of our build.old.bat
# the only difference is that console logging goes to a file (build.log)

$buildloc = "_build_3rdparty_target"
$relBuildloc = "..\$buildloc"
$startloc = (Get-Item -Path ".\" -Verbose).FullName
$emptyDir = "emptyDir"

Start-Transcript -path "build $((Get-Date).ToString('yyyy-dd-MM_hh-mm-ss')).log"

# too bad Remove-Item has a limitation when it comes to string length
# If (Test-Path $relBuildloc) {
#	Remove-Item -Force -Recurse $RelBuildloc
#}
# this works
# cmd /C "rmdir /S /Q $RelBuildloc"  2>&1 | out-host
# this is a nice trick
New-Item -ItemType Directory -Force -Path $emptyDir
robocopy $emptyDir $relBuildloc /ndl /nc /ns /nfl /e /s /mir /purge > $null
Remove-Item -Force $emptyDir
Remove-Item -Force -Recurse $relBuildloc

New-Item -ItemType Directory -Force -Path $relBuildloc
#mkdir $relBuildloc

#Get-Childitem * -recurse | %{
#                   Copy-Item -Path $_ -Destination $relBuildloc}
# Copy-Item * $relBuildloc -recurse
robocopy . $relBuildloc /E /ndl /nc /ns /nfl 2>&1 | out-host
# /np /njh /njs

Set-Location $relBuildloc

mvn p2:site 2>&1 | out-host

# if you remove -Pjacoco you'll need to adjust the commandlines for surefire and remove ${tycho.testArgLine}

Stop-Transcript
Set-Location $startloc 