@ECHO OFF
@echo #########################################################
@echo ##                    D  I  D                          ##
@echo ##                    Y  O  U                          ##
@echo ##          DISABLE YOUR VIRUS SCANNER ?????           ##
@echo #########################################################
pause
rem this file just wraps our powershell script so it can do proper logging
rem see http://blog.danskingdom.com/allow-others-to-run-your-powershell-scripts-from-a-batch-file-they-will-love-you-for-it/
rem for details
SET ThisScriptsDirectory=%~dp0
SET PowerShellScriptPath=%ThisScriptsDirectory%build.logged.ps1
PowerShell -NoProfile -ExecutionPolicy Bypass -Command "& {Start-Process PowerShell -ArgumentList '-NoProfile -ExecutionPolicy Bypass -File ""%PowerShellScriptPath%""' -Verb RunAs}";